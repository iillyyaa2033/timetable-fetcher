package com.iillyyaa2033.timetablefetcher.eltech;

import com.iillyyaa2033.timetablefetcher.data.CalendarEvent;
import com.iillyyaa2033.timetablefetcher.data.DateBounds;
import com.iillyyaa2033.timetablefetcher.data.Timetable;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Основные предположения:
 * - дни задаются полными названиями ("понедельник", "вторник" и так далее)
 * - начала пар задаются в форматах ч.мм или чч.мм
 * - ячейка дней по высоте охватывает все ячейки начала пар этого дня
 * - ячейка пар по высоте охватывает все ячейки инфы об этой паре (т.е. 4 ячейки)
 * - расписание для одной группы содержится в одном столбце
 * - инфа о паре занимает ровно 4 ряда
 * - пары могут быть двух типов: еженедельные и по четным/нечетным неделям
 * - пары могут быть индивидуальными для группы - занимают один столбец:
 *      - еженедельные пары состоят из заголовка (2 ряда), препода (1 ряд) и кабинета (1 ряд)
 *      - чредующиеся пары занимают по 2 ряда: заголовок (1 ряд) и препод + кабинет (1 ряд)
 * - пары могут быть общими для части групп - занимают несколько столбцов:
 *      - еженедельные пары состоят из заголовка (2 ряда, несколько столбцов), кабинета (3й ряд, первый столбец группы), препода (3й ряд, следующие столбцы группы)
 *      - чредующиеся пары - аналогично: заголовок (1 ряд, неск. столбцов), кабинет (2й ряд, 1й стобец), препод (2й ряд, след. столбцы)
 * - если все соответствующие ячейки еженедельной или чредующейся пары пусты, считается, что пары в это время нет
 * - если некоторые ячейки пары пусты, считается ошибкой
 *
 * - номер кабинета задается четырьмя цифрами
 */
public class EltechTimetable extends Timetable {

    public static final long ZERO_WEEK_START = 1566766800000L;
    public static final int WEEK_COUNT = 19;

    public static final String KEY_FILE = "eltech_file";
    public static final String KEY_GROUP = "eltech_group";


    // Проверяем, что в текст ячейки - это именно время:
    // набор из одной или двух цифр, затем точка, запятая, двоеточие, или точка с запятой, затем еще две цифры
    private static final String hourPattern = "^\\d?\\d[.,:;]\\d\\d$";

    private static final long MILLIS_IN_DAY = 24 * 60 * 60 * 1000;

    private Sheet sheet;
    private List<CellRangeAddress> regionsList = new ArrayList<>();

    // Parse results
    private int[] dayStarts = new int[7];
    private int[] dayLengths = new int[7];
    private DaypairsInfo[] days = new DaypairsInfo[7];

    private ArrayList<EltechPair> pairs = new ArrayList<>();

    public EltechTimetable(Map<String, String> params, Map<String, String> shortens) {
        super(params, shortens);
    }

    @Override
    public void prepare() throws Exception {


        File file = new File(getParams().get(KEY_FILE));
        String groupName = getParams().get(KEY_GROUP);


        for(int i = 0; i < days.length; i++){
            days[i] = new DaypairsInfo();
        }

        Workbook book = WorkbookFactory.create(file);
        sheet = book.getSheetAt(0);

        for(int i = 0; i < sheet.getNumMergedRegions(); i++) {
            regionsList.add(sheet.getMergedRegion(i));
        }


        int daysColumn = searchDaysColumn();
        if(daysColumn < 0) throw new RuntimeException("Cannot find days column");

        fillDaysInfo(daysColumn);

        int hoursColumn = searchHoursColumn();
        if(hoursColumn < 0) throw new RuntimeException("Cannot find hours column");

        fillDaypairs(hoursColumn);

        int groupColumn = searchGroupColumn(groupName);
        if(groupColumn < 0) throw new RuntimeException("Cannot find group column");

        generatePairs(groupColumn);
    }

    @Override
    public ArrayList<CalendarEvent> getEventsAt(DateBounds bounds) {
        ArrayList<CalendarEvent> result = new ArrayList<>();
        for(EltechPair pair : pairs){

            for(Long start : pair.getStarts()){

                if(bounds.contains(start)){

                    result.add(
                            new CalendarEvent(-1,-1, pair.getTitle(getShortens()), start,
                                    pair.getDuration(), pair.getDesctiption(), pair.getLocation()));
                }
            }
        }

        return result;
    }


    /**
     * Ищем колонку с днями
     *
     * @return int
     */
    private int searchDaysColumn(){
        // Parameters
        int successCoef = 2; // Сколько приплюсовываем при совпадении
        int failCoef = 1; // Сколько отнимаем при несовпадении
        int minCol = 0; // Ограничения по рядам и строкам
        int maxCol = 10;
        int minRow = 0;
        int maxRow = 255;

        String[] days = {
                "понедельник",
                "вторник",
                "среда",
                "четверг",
                "пятница",
                "суббота",
                "воскресенье"
        };


        // Code
        int foundColumn = -1;
        int foundScore = 3; // Типа допуск

        for(int colNum = minCol; colNum <= maxCol; colNum++){
            int score = 0;

            for(int rowNum = minRow; rowNum <= maxRow; rowNum++){
                Row row = sheet.getRow(rowNum);
                if(row == null) continue;

                Cell cell = row.getCell(colNum);
                if(cell == null) continue;

                String value = cell.toString().trim();

                if(!value.isEmpty()){
                    if(inArrayIgnoreCase(days, value)) score += successCoef;
                    else score -= failCoef;
                }
            }

            if(score > foundScore){
                foundColumn = colNum;
                foundScore = score;
            }
        }


        return foundColumn;
    }

    /**
     * Заполняем инфу о днях
     *
     * @param column int
     */
    private void fillDaysInfo(int column){
        // Parameters
        int minRow = 0; // Ограничения по строкам
        int rowCnt = 500;

        // Code

        for (int rowNum = minRow; rowNum <= minRow+rowCnt; rowNum++) {
            Row row = sheet.getRow(rowNum);
            if (row == null) continue;

            Cell cell = row.getCell(column);
            if (cell == null) continue;

            String value = cell.toString().trim();
            int height = getMergeHeight(column, rowNum);

            if(!value.isEmpty()) {
                int selectedDay;
                switch (value.toLowerCase()) {
                    case "понедельник":
                        selectedDay = 0;
                        break;
                    case "вторник":
                        selectedDay = 1;
                        break;
                    case "среда":
                        selectedDay = 2;
                        break;
                    case "четверг":
                        selectedDay = 3;
                        break;
                    case "пятница":
                        selectedDay = 4;
                        break;
                    case "суббота":
                        selectedDay = 5;
                        break;
                    case "воскресенье":
                        selectedDay = 6;
                        break;
                    default:
                        selectedDay = -1;
                }

                if(selectedDay != -1){
                    dayStarts[selectedDay] = rowNum;
                    dayLengths[selectedDay] = height;
                }

            }
        }

    }

    /**
     * Ищем колонку с началами пар
     *
     * @return int
     */
    private int searchHoursColumn(){
        // Parameters
        int successCoef = 1; // Сколько приплюсовываем при совпадении
        int failCoef = 4; // Сколько отнимаем при несовпадении
        int minCol = 0; // Ограничения по рядам и строкам
        int maxCol = 10;
        int minRow = 0;
        int maxRow = 255;


        // Code
        int foundColumn = -1;
        int foundScore = 3; // Типа допуск

        for(int colNum = minCol; colNum <= maxCol; colNum++) {
            int score = 0;

            for (int rowNum = minRow; rowNum <= maxRow; rowNum++) {
                Row row = sheet.getRow(rowNum);
                if (row == null) continue;

                Cell cell = row.getCell(colNum);
                if (cell == null) continue;

                String value = cell.toString().trim();

                if(!value.isEmpty()){
                    if(value.matches(hourPattern)) score += successCoef;
                    else score -= failCoef;
                }
            }

            if(score > foundScore){
                foundColumn = colNum;
                foundScore = score;
            }
        }

        return foundColumn;
    }

    /**
     * Заполняем начала пар
     *
     * @param column int
     */
    private void fillDaypairs(int column){
        // Parameters
        int minRow = 0; // Ограничения по строкам
        int rowCnt = 500;

        // Code
        for (int rowNum = minRow; rowNum <= minRow+rowCnt; rowNum++) {

            Row row = sheet.getRow(rowNum);
            if (row == null) continue;

            Cell cell = row.getCell(column);
            if (cell == null) continue;

            int dayIdx = getDayIdx(rowNum);
            if (dayIdx == -1) continue;

            String value = cell.toString().trim();
            int mergeHeight = getMergeHeight(column, rowNum);

            if(!value.isEmpty()) { // Ячейка не пустая

                if(value.matches(hourPattern)){ // Все в порядке, задаем новые значения
                    days[dayIdx].pairRowStart.add(rowNum);
                    days[dayIdx].pairRowLen.add(mergeHeight);
                    days[dayIdx].pairTimeStart.add(parsePairStart(value));
                }
            }

            rowNum += mergeHeight - 1;
        }
    }

    /**
     * Ищем ячейку с именем группы
     *
     * @param group имя группы
     * @return int[]{столбец, строка}
     */
    private int searchGroupColumn(String group){

        int minCol = 0; // Ограничения по рядам и строкам
        int maxCol = 64;
        int minRow = 0;
        int maxRow = 255;

        for(int rowNum = minRow; rowNum <= maxRow; rowNum++){
            Row row = sheet.getRow(rowNum);
            if(row == null) continue;

            for(int colNum = minCol; colNum <= maxCol; colNum++) {
                Cell cell = row.getCell(colNum);
                if (cell == null) continue;

                String value = cell.toString().trim();
                if(value.equalsIgnoreCase(group)){
                    return colNum;
                }
            }
        }

        return -1;
    }

    private void generatePairs(int column){
        for(int day = 0; day < 7; day++){
            DaypairsInfo di = days[day];

            for(int i = 0; i < di.pairRowStart.size(); i++){

                int pairStart = di.pairRowStart.get(i);
                long pairTime = di.pairTimeStart.get(i);

                if(!isEmpty(column, pairStart, 4)){

                    int firstMergeWidth = getMergeWidth(column, pairStart);
                    int firstMergeHeight = getMergeHeight(column, pairStart);

                    // Проверим на нормальную еженедельную пару
                    if(firstMergeWidth == 1 && firstMergeHeight == 2){
                        Cell title = getOriginalCell(column, pairStart);

                        if(title != null){
                            if(!title.toString().isEmpty()) {
                                pairs.add(new EltechPair(
                                        day * MILLIS_IN_DAY + pairTime,
                                        title.toString(),
                                        getOriginalCell(column, pairStart+2) +" "+getOriginalCell(column, pairStart+3)
                                ));
                            }
                        } else {
                            System.out.println("! ОШИБКА !");
                        }

                        // Оказалось, что это не нормальная пара
                    } else {

                        // Это та же еженедельная пара, но для всего потока
                        if(firstMergeWidth > 1 && firstMergeHeight == 2){

                            Cell title = getOriginalCell(column, pairStart);

                            if(title != null){
                                if(title.toString().isEmpty()){
                                    System.out.println("! ОШИБКА !");
                                } else {

                                    pairs.add(new EltechPair(
                                            day * MILLIS_IN_DAY + pairTime,
                                            title.toString(),
                                            getOriginalCell(title.getColumnIndex() + 1, pairStart+2)+" "+getOriginalCell(title.getColumnIndex(), pairStart+2)

                                    ));
                                }
                            } else {
                                System.out.println("! ОШИБКА !");
                            }

                            // Еженедельная пара всего потока, но без каких-л. примечаний
                        } else if(firstMergeWidth > 1 && firstMergeHeight == 4){

                            pairs.add(new EltechPair(
                                    day * MILLIS_IN_DAY + pairTime,
                                    ""+getOriginalCell(column, pairStart)
                            ));

                            // Чредующиеся пары
                        } else {
                            parseHalf(column, pairStart, pairTime, true);
                            parseHalf(column, pairStart+2, pairTime,false);
                        }
                    }
                }
            }
            System.out.println();
        }
    }

    /**
     * Парсим половинку пары
     *
     * @param column c
     * @param pairStart ps
     */
    private void parseHalf(int column, int pairStart, long pairTime, boolean upper){
        int firstMergeWidth = getMergeWidth(column, pairStart);
        int firstMergeHeight = getMergeHeight(column, pairStart);

        if(firstMergeWidth <= 1 && firstMergeHeight <= 1){
            String f = getOriginalCell(column, pairStart)+"";
            String s = getOriginalCell(column, pairStart+1)+"";

            if((f.isEmpty() || f.equals("null")) && (s.isEmpty() || s.equals("null"))) {
                // Do nothing
            } else if(!f.isEmpty()){
                pairs.add(new EltechPair(
                        pairTime,
                        upper ? EltechPair.TYPE_FIRST : EltechPair.TYPE_SECOND,
                        f,
                        s));
            } else {
                System.out.print("! ОШИБКА !");
            }
        } else if(firstMergeHeight <= 1) {
            Cell title = getOriginalCell(column, pairStart);

            pairs.add(new EltechPair(
                    pairTime,
                    upper ? EltechPair.TYPE_FIRST : EltechPair.TYPE_SECOND,
                    title.toString(),
                    getOriginalCell(title.getColumnIndex() + 1, pairStart+1) +" "+getOriginalCell(title.getColumnIndex(), pairStart+1)));

        } else {
            System.out.print("! ОШИБКА !");
        }
    }

    // ===== //
    // Utils //
    // ===== //
    private boolean inArrayIgnoreCase(String[] array, String value){
        for(String s : array){
            if(s.equalsIgnoreCase(value)){
                return true;
            }
        }

        return false;
    }

    /**
     * Переводим строку в миллисекунды с начала суток
     *
     * @param value string
     * @return long
     */
    private long parsePairStart(String value){
        String v = value
                .replace(":", ".")
                .replace(",", ".")
                .replace(";",".");
        String[] split = v.split("\\.");

        return Integer.parseInt(split[0]) * 60 * 60 * 1000 + Integer.parseInt(split[1]) * 60 * 1000;
    }

    /**
     * Определяем, к какому дню принадлежит ряд
     * @param row r
     * @return r
     */
    private int getDayIdx(int row){

        for(int i = 0; i < 7; i++){
            if(row >= dayStarts[i] && row < dayStarts[i] + dayLengths[i])
                return i;
        }

        return -1;
    }

    /**
     * Находим значащую ячейку с учетом возможного merge by width
     *
     * Значащая - это в которой есть данные.
     * @param column col
     * @param row row
     * @return cell
     */
    private Cell getOriginalCell(int column, int row){
        Cell cell = null;

        for(CellRangeAddress region : regionsList) {

            // If the region does contain the cell you have just read from the row
            if(region.isInRange(row, column)) {
                // Now, you need to get the cell from the top left hand corner of this
                int rowNum = region.getFirstRow();
                int colIndex = region.getFirstColumn();

                if(rowNum != row) break;

                Row rowObj = sheet.getRow(rowNum);
                if (rowObj == null) break;

                cell = rowObj.getCell(colIndex);
                break;
            }
        }

        if(cell == null){
            Row rowObj = sheet.getRow(row);
            if (rowObj != null)
                cell = rowObj.getCell(column);
        }

        return cell;
    }

    private int getMergeWidth(int column, int row){
        for(CellRangeAddress region : regionsList) {

            if (region.isInRange(row, column)) {
                return region.getLastColumn() - region.getFirstColumn() + 1;
            }
        }

        return 0;
    }

    private int getMergeHeight(int column, int row){
        for(CellRangeAddress region : regionsList) {

            if (region.isInRange(row, column)) {
                return region.getLastRow() - region.getFirstRow() + 1;
            }
        }

        return 0;
    }

    private boolean isEmpty(int column, int start, int limit){
        for(int i = start; i < start+limit; i++){
            if(getMergeHeight(column, i) > 1 || getMergeWidth(column, i) > 1) return false;

            Row row = sheet.getRow(i);
            if(row == null) continue;

            Cell cell = row.getCell(column);
            if(cell == null) continue;

            if(!cell.toString().trim().isEmpty()) return false;
        }

        return true;
    }

    /**
     * Описывает какие пары есть в этом дне + их соответствие рядам
     */
    class DaypairsInfo {

        ArrayList<Integer> pairRowStart = new ArrayList<>(); // С какого ряда начинается пара
        ArrayList<Integer> pairRowLen = new ArrayList<>(); // Сколько рядов занимает пара
        ArrayList<Long> pairTimeStart = new ArrayList<>(); // Во сколько пара начинается

    }
}
