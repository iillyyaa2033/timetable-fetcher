package com.iillyyaa2033.timetablefetcher;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract;

public class CalendarCleanerTask extends AsyncTask<Void, Void, Exception> {

    private Context context;
    private AlertDialog d;

    CalendarCleanerTask(Context context){
        this.context = context;
        d = new AlertDialog.Builder(context).setMessage("Рвботаем").setCancelable(false).show();
    }

    @Override
    protected Exception doInBackground(Void... voids) {

        String[] projection = new String[] {
                CalendarContract.Events._ID,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.CUSTOM_APP_PACKAGE
        };

        String selection = "(" + CalendarContract.Events.CUSTOM_APP_PACKAGE + " == \""+ context.getPackageName()+"\""
                + " ) AND ( deleted != 1 )";

        Cursor cursor = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null);

        if (cursor!=null&&cursor.getCount()>0&&cursor.moveToFirst()) {
            do {
                Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, cursor.getLong(0));
                context.getContentResolver().delete(deleteUri, null, null);
            } while ( cursor.moveToNext());
            cursor.close();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Exception e) {
        d.dismiss();
        String message = "Очистка календаря завершена ";


        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Окей", null)
                .show();
    }
}
