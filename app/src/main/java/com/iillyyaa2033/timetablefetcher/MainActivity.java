package com.iillyyaa2033.timetablefetcher;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.iillyyaa2033.timetablefetcher.data.CalendarInfo;
import com.iillyyaa2033.timetablefetcher.data.DateBounds;
import com.iillyyaa2033.timetablefetcher.eltech.EltechTimetable;
import com.iillyyaa2033.timetablefetcher.sut.SutTimetable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {

    private static CalendarInfo selectedCalendar;

    private Map<String, String> params = new HashMap<>();

    private Button queryBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queryBtn = findViewById(R.id.main_btn_query);
        queryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSettings();
            }
        });

        String savedQuery = PreferenceManager.getDefaultSharedPreferences(this).getString("bonch_query",null);
        if(savedQuery != null) params.put(SutTimetable.KEY_REQUEST, savedQuery);

        String eltechFile = PreferenceManager.getDefaultSharedPreferences(this).getString(EltechTimetable.KEY_FILE,null);
        if(eltechFile != null) params.put(EltechTimetable.KEY_FILE, eltechFile);

        String eltechGroup = PreferenceManager.getDefaultSharedPreferences(this).getString(EltechTimetable.KEY_GROUP,null);
        if(eltechGroup != null) params.put(EltechTimetable.KEY_GROUP, eltechGroup);


        findViewById(R.id.main_btn_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateBounds bounds = null;

                int item = ((Spinner) findViewById(R.id.main_spinner_bounds)).getSelectedItemPosition();
                switch(item){
                    case 0:
                        bounds = DateBounds.currentAndNextWeek();
                        break;
                    case 1:
                        bounds = DateBounds.allAvailable();
                        break;
                }

                if(((CheckBox) findViewById(R.id.fdel)).isChecked()) {
                    params.put(CalendarFillerTask.KEY_FORCE_DELETE, "yup");
                } else {
                    params.remove(CalendarFillerTask.KEY_FORCE_DELETE);
                }


                CalendarFillerTask cft = new CalendarFillerTask(MainActivity.this, selectedCalendar, bounds);
                cft.execute(params);
            }
        });

        setupVuzSpinner();
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean permGranted = true;

        try {
            if (Build.VERSION.SDK_INT > 23) {
                if (checkSelfPermission("android.permission.WRITE_CALENDAR") != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission("android.permission.READ_CALENDAR") != PackageManager.PERMISSION_GRANTED) {
                    permGranted = false;
                    requestPermissions(new String[]{"android.permission.READ_CALENDAR","android.permission.WRITE_CALENDAR"}, 243);
                }
            }
        } catch(Exception e){
            userErrorMessage(e);
        }

        if(permGranted) setupCalendarSpinner();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(grantResults[0] != PackageManager.PERMISSION_GRANTED
                && grantResults[1] != PackageManager.PERMISSION_GRANTED){
            finish();
        } else {
            setupCalendarSpinner();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0,0,0,"Очистить календари");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case 0:
                showCleanCalendars();
                break;
        }
        return true;
    }


    void setupVuzSpinner(){
        Spinner sp = findViewById(R.id.vuz_selector);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position){
                    case 0:
                        params.put(CalendarFillerTask.KEY_MODE, "sut");
                        params.put(CalendarFillerTask.KEY_SHORTENS, "shortens-sut.json");
                        break;

                    case 1:
                        params.put(CalendarFillerTask.KEY_MODE, "eltech");
                        params.put(CalendarFillerTask.KEY_SHORTENS, "shortens-eltech.json");
                        break;
                }

                updateSettingsButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void setupCalendarSpinner(){
        String rememberedCalendarId = PreferenceManager.getDefaultSharedPreferences(this).getString("MAIN_UI_CAL_ID", null);

        Spinner sp = findViewById(R.id.main_spinner_calendar);

        ArrayList<CalendarInfo> calendars = new ArrayList<>();
        int savedSelectedCalendarId = -1;

        try {
            ContentResolver contentResolver = this.getContentResolver();
            final Cursor cursor = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"),
                    new String[]{"_id", CalendarContract.Calendars.CALENDAR_DISPLAY_NAME}, null, null, null);

            int index = 0;
            while (cursor.moveToNext()) {
                final String _id = cursor.getString(0);
                final String displayName = cursor.getString(1);
                calendars.add(new CalendarInfo(_id, displayName));
                if (_id.equals(rememberedCalendarId)) savedSelectedCalendarId = index;
                index++;
            }
        } catch (Exception e) {
            userErrorMessage(e);
        }

        if(calendars.size() > 0) selectedCalendar = calendars.get(0);

        final ArrayAdapter<CalendarInfo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, calendars);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCalendar = adapter.getItem(position);
                PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().putString("MAIN_UI_CAL_ID",""+selectedCalendar.id).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(savedSelectedCalendarId > -1) sp.setSelection(savedSelectedCalendarId);
    }

    void updateSettingsButton(){
        if(!params.containsKey(CalendarFillerTask.KEY_MODE)) return;

        switch (params.get(CalendarFillerTask.KEY_MODE)) {
            case "sut":
                queryBtn.setText(params.containsKey(SutTimetable.KEY_REQUEST) ? "[Выбрано пользователем]" : "[Параметры по умолчанию]");
                break;
            case "eltech":
                queryBtn.setText(params.containsKey(EltechTimetable.KEY_FILE) ? "[Выбрано пользователем]" : "Не выбрано");
                break;
            default:
                queryBtn.setText("Нажми меня");
        }
    }


    void showSettings(){
        if(!params.containsKey(CalendarFillerTask.KEY_MODE)) return;

        switch (params.get(CalendarFillerTask.KEY_MODE)) {
            case "sut":
                showBonchSettings();
                break;
            case "eltech":
                showEltechSettings();
                break;
            default:
                Toast.makeText(this, "Unable to find settings", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    void showBonchSettings(){
        String selector = "https://cabinet.sut.ru/raspisanie_all_new";

        WebView view = new WebView(this);
        view.getSettings().setJavaScriptEnabled(true);

        final AlertDialog dialog = new AlertDialog.Builder(this).setView(view).show();

        WebViewClient webViewClient = new WebViewClient(){

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {


                if (url.contains(".css")) {
                    return new WebResourceResponse(
                            "text/css",
                            "UTF-8",
                            getResources().openRawResource(R.raw.empty));
                } else {
                    return super.shouldInterceptRequest(view, url);
                }
            }

            @Override
            public void onLoadResource(WebView view, String url){
                if(url.startsWith("https://cabinet.sut.ru/raspisanie_all_new?")){
                    params.put(SutTimetable.KEY_REQUEST, url);
                    dialog.dismiss();
                    PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().putString("bonch_query", url).apply();

                    updateSettingsButton();
                }
            }

            @Override
            public void onPageFinished(WebView web, String url) {
                web.loadUrl("javascript:(" +
                        "function(){" +
                        "document.getElementsByTagName(\"form\")[0].method=\"get\";" +
                        "})()");
            }


        };

        view.setWebViewClient(webViewClient);
        view.loadUrl(selector);
    }

    void showEltechSettings(){

        final View root = LayoutInflater.from(this).inflate(R.layout.settings_eltech, null, false);

        final EditText t = root.findViewById(R.id.path);
        final EditText g = root.findViewById(R.id.group);

        t.setText(params.get(EltechTimetable.KEY_FILE));
        g.setText(params.get(EltechTimetable.KEY_GROUP));

        new AlertDialog.Builder(this)
                .setView(root)
                .setPositiveButton("Ок", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        params.put(EltechTimetable.KEY_FILE, t.getText().toString());
                        params.put(EltechTimetable.KEY_GROUP, g.getText().toString());

                        PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit()
                                .putString(EltechTimetable.KEY_FILE, t.getText().toString())
                                .putString(EltechTimetable.KEY_GROUP, g.getText().toString())
                                .apply();

                        updateSettingsButton();
                    }
                })
                .setNegativeButton("Отмена", null)
                .show();

    }

    void showCleanCalendars(){
        new AlertDialog.Builder(this)
                .setMessage("Это удалит все записи (созданные программой) изо всех календарей.\n\nПродолжить?")
                .setNegativeButton("Нет",null)
                .setPositiveButton("Ага", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new CalendarCleanerTask(MainActivity.this).execute();
                    }
                })
                .show();
    }

    private void userErrorMessage(Exception e){
        String message = "Got exception: "+e.getMessage();

        for(StackTraceElement el : e.getStackTrace()){
            message += "\n"+ el.toString();
        }

        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Ну, бывает", null)
                .show();
    }

}
