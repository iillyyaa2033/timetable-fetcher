package com.iillyyaa2033.timetablefetcher;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.widget.Toast;

import com.iillyyaa2033.timetablefetcher.data.CalendarEvent;
import com.iillyyaa2033.timetablefetcher.data.CalendarInfo;
import com.iillyyaa2033.timetablefetcher.data.DateBounds;
import com.iillyyaa2033.timetablefetcher.data.Timetable;
import com.iillyyaa2033.timetablefetcher.eltech.EltechTimetable;
import com.iillyyaa2033.timetablefetcher.sut.SutTimetable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

public class CalendarFillerTask extends AsyncTask<Map<String, String>, Void, Exception> {

    public static final String KEY_FORCE_DELETE = "FORCE_DELETE";
    public static final String KEY_SHORTENS = "SHORTENS";
    public static final String KEY_MODE = "MODE";

    private final DateBounds bounds;
    private final CalendarInfo selectedCalendar;

    private Context context;
    private AlertDialog d;

    CalendarFillerTask(Context context, CalendarInfo calendar, DateBounds bounds){
        this.context = context;
        this.selectedCalendar = calendar;
        this.bounds = bounds;
    }

    @Override
    protected void onPreExecute() {
        d = new AlertDialog.Builder(context).setMessage("Работаем...").create();
        d.show();
    }

    @Override
    protected Exception doInBackground(Map<String, String>... params) {
        Map<String, String> pms = params[0];
        Timetable timetable = getTT(params[0]);

        try {
            timetable.prepare();

            for(CalendarEvent oldEvent : getCurrentEvents()) {
                if (pms.containsKey(KEY_FORCE_DELETE)) {
                    deleteEvent(oldEvent.id);
                }
            }

            for(CalendarEvent ev : timetable.getEventsAt(bounds)){
                insertOrUpdate(ev);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return e;
        }

        return null;
    }

    private Timetable getTT(Map<String, String> params){
        if(params.containsKey(CalendarFillerTask.KEY_MODE)) {
            switch (params.get(CalendarFillerTask.KEY_MODE)) {
                case "sut":
                    return new SutTimetable(params, getShortens(params.get(KEY_SHORTENS)));
                case "eltech":
                    return new EltechTimetable(params, getShortens(params.get(KEY_SHORTENS)));
            }
        }

        return null;
    }

    private void insertOrUpdate(CalendarEvent event){

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, event.start);
        values.put(CalendarContract.Events.DTEND, event.start + event.duration);
        values.put(CalendarContract.Events.TITLE, event.title);
        values.put(CalendarContract.Events.DESCRIPTION, event.description);
        values.put(CalendarContract.Events.CALENDAR_ID, selectedCalendar.id);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        values.put(CalendarContract.Events.EVENT_LOCATION, event.location);
        values.put(CalendarContract.Events.CUSTOM_APP_PACKAGE, context.getPackageName());
        values.put(CalendarContract.Events.CUSTOM_APP_URI, "event://details?data="+event.toJson());

        boolean updated = false;

        for(CalendarEvent oldEvent : getCurrentEvents()) {
            if(oldEvent.atSameDate(event)){
                updateEvent(values, oldEvent.id);
                updated = true;
            }
        }

        if(!updated) insertEvent(values);
    }

    private ArrayList<CalendarEvent> getCurrentEvents(){

        ArrayList<CalendarEvent> list = new ArrayList<>();

        String[] projection = new String[] {
                CalendarContract.Events._ID,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.CUSTOM_APP_PACKAGE
        };

        String selection = "( " + CalendarContract.Events.CALENDAR_ID + " == " + selectedCalendar.id
                + " ) AND ( " + CalendarContract.Events.DTSTART + " >= " + bounds.startMillis
                + " ) AND ( " + CalendarContract.Events.DTSTART + " <= " + bounds.endMillis
                + " ) AND ( " + CalendarContract.Events.CUSTOM_APP_PACKAGE + " == \""+ context.getPackageName()+"\""
                + " ) AND ( deleted != 1 )";

        Cursor cursor = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null);

        if (cursor!=null&&cursor.getCount()>0&&cursor.moveToFirst()) {
            do {
                list.add(new CalendarEvent(cursor.getLong(0), cursor.getLong(1), cursor.getString(2), cursor.getLong(3)));
            } while ( cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    private void insertEvent(ContentValues values){
        context.getContentResolver().insert(CalendarContract.Events.CONTENT_URI, values);
    }

    private void updateEvent(ContentValues values, long replacementId){
        context.getContentResolver().update(CalendarContract.Events.CONTENT_URI, values, CalendarContract.Events._ID+" == "+replacementId, null);
    }

    private void deleteEvent(long eventID){
        Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
        context.getContentResolver().delete(deleteUri, null, null);

    }

    @Override
    protected void onPostExecute(Exception result) {
        d.dismiss();

        String message = "Запись в календарь завершена ";

        if(result == null){
            message += "успешно";
        } else {
            message += "со следующей ошибкой:";
            message += "\n"+result.getMessage();
        }

        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Окей", null)
                .show();
    }

    private Map<String, String> getShortens(String filename)  {

        HashMap<String, String> result = new HashMap<>();

        String rawJson = "";


        InputStream is = null;
        try {
            is = context.getAssets().open(filename);

            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            rawJson = s.hasNext() ? s.next() : "";

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(is != null) is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            JSONObject json = new JSONObject(rawJson);

            Iterator<String> i = json.keys();

            while (i.hasNext()){
                String key = i.next();

                result.put(key.toLowerCase(), json.getString(key));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return result;
    }
}
