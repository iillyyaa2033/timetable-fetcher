package com.iillyyaa2033.timetablefetcher.sut;

import java.util.Map;

public class SutPair {

    private final int[] weeks;
    private final int weekday;
    private final int pair;

    private final String subject;
    private final String type;
    private final String teacher;
    private final String aud;

    private SutPair(int weekday, int pair, String subject, String type, int[] weeks, String teacher, String aud) {
        this.weekday = weekday;
        this.pair = pair;
        this.subject = subject;
        this.type = type;
        this.weeks = weeks;
        this.teacher = teacher;
        this.aud = aud;
    }

    static SutPair parse(String weekday, String pair, String subject, String type, String weeks, String teacher, String aud) {

        int pWeekday = -1;
        int pPair = -1;
        int[] pWeeks = new int[0];

        try {
            pWeekday = Integer.parseInt(weekday);
        } catch (Exception e) {
        }

        try {
            pPair = Integer.parseInt(pair);
        } catch (Exception e) {
        }

        try {
            pWeeks = parseDirtyArray(weeks);
        } catch (Exception e) {
        }

        return new SutPair(pWeekday, pPair, subject, type, pWeeks, teacher, aud.substring(6));
    }

    private static int[] parseDirtyArray(String data) {
        String clearData = data.replaceAll("[^\\d\\s]", "");
        String[] stringArray = clearData.split(" ");
        int[] output = new int[stringArray.length];

        for(int i = 0; i < output.length; i++) {
            try {
                output[i] = Integer.parseInt(stringArray[i]);
            } catch (Exception e) {
                System.err.println("Error while parsing dirty array");
            }
        }

        return output;
    }

    private static String a2s(int[] array) {
        String result = "";

        for(int i = 0; i < array.length; i++) {
            result += array[i] + "|";
        }

        if(result.length() > 0)
            result = result.substring(0, result.length() - 1);

        return result;
    }

    private static long asMillis(int week, int weekday, int pair){
        long result = SutTimetable.hardcodedStart;

        long millisInDay = 1000 * 60 * 60 * 24;
        long millisInWeek = millisInDay * 7;

        result += (week-1) * millisInWeek;
        result += (weekday-1) * millisInDay;
        result += pairInMillisOffset(pair);

        return result;
    }

    /**
     * По какой-то причине во втором семестре 18/19 учебного года номера
     * регулярных пар (1-5) смещены на единицу вниз (т.е. 2-6).
     * <br/><br/>
     * Это поле - костыль на случай, если регулярные пары опять куда-то
     * сместятся.
     * @author: iillyyaa2033
     */
    final static int regularPairOffset = 1;
    private static long pairInMillisOffset(int pair){

        switch(pair) {
            case 1+regularPairOffset: return timeOffset(9,0);
            case 2+regularPairOffset: return timeOffset(10,45);
            case 3+regularPairOffset: return timeOffset(13,0);
            case 4+regularPairOffset: return timeOffset(14,45);
            case 5+regularPairOffset: return timeOffset(16,30);
            case 85: return timeOffset(12, 0);
            case 87: return timeOffset(15, 0);
            default: return 0;
        }
    }

    private static long timeOffset(int hour, int minutes){
        return hour * 60 * 60 * 1000 + minutes * 60 * 1000;
    }

    private static long durationByPairNumber(int pair){
        switch(pair){
            case 85:
                return timeOffset(1, 30);
            default:
                return timeOffset(1, 35);
        }
    }

    private static String abbrSubject(Map<String, String> shortens, String subj){
        if(shortens.containsKey(subj.toLowerCase())){
            return shortens.get(subj.toLowerCase());
        } else {
            System.out.println("No shorten for "+subj.toLowerCase());
            return subj;
        }
    }

    private static String abbrType(String type){
        switch (type.toLowerCase()){
            case "(лекция)": return "лк";
            case "(практические занятия)": return "пр";
            case "(лабораторная работа)": return "лаб";
            case "()": return "";
            default: return type;
        }
    }

    private static String abbrAddr(String location){
        return location
                .replace("-Б22","")
                .replace("; Б22","");
    }


    @Override
    public String toString() {
        return weekday + "/" + pair + " - " + subject + " " + type + " - " + teacher + " " + aud +" ["+ a2s(weeks)+"]";
    }

    public String getTitle(Map<String, String> shortens){
        return SutPair.abbrSubject(shortens, subject)+ " " + SutPair.abbrType(type);
    }

    public String getDesctiption(){
        return "Препод: "+teacher;
    }

    public String getLocation(){
        return SutPair.abbrAddr(aud);
    }

    public long[] getAllStarts(){
        long[] result = new long[weeks.length];

        for(int i = 0; i < result.length; i++){
            result[i] = asMillis(weeks[i], weekday, pair);
        }

        return result;
    }

    public long getDuration(){
        return SutPair.durationByPairNumber(pair);
    }


}
